import html from 'choo/html'

export default (state, prev, send) =>
  html`
    <section id="main-view" class=${state.isSnapping ? 'faded' : ''}>

      ${
        !state.firstTime && state.label
        ? html`
          <div class="row">
            <h2>${state.label}</h2>
            <h4>Javanese</h4>
          </div>
        `
        : null
      }
      ${
        state.cameraReady
        ? html`
          <div
            id="shutter-button"
            class="${state.isSnapping ? 'busy' : ''}"
            onclick=${_ => send('snap')}>
          </div>
        `
        : null
      }
      ${
        state.firstTime && state.cameraReady
        ? html`<h5 id="first-time">Try taking a picture of Javanese Script.</h5>`
        : null
      }
      <form class="debug" id="form-image" >
         <input type='file' id="imgInp" accept="image/gif, image/jpeg"
                onchange=${_ => send('readIm')}>
         />
      </form>
      <div class="debug">${state.guesses}</div>
    </section>
  `
