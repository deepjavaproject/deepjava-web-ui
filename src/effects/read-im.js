import xhr from 'xhr'
import {apiUrls} from '../config'
import * as fs from 'fs';

const breakPoint = 800
const canvSize   = 640
const targetPct  = 0.7
const targetTop  = 0.4

export default function readIm(state, input, send, done) {
  send('startSnap', done)
  console.log("Test : "+input)
  console.log("Test 2: %o",state)

  fs.readFile(im_loc, function(err, data) {
        if (err) throw err;
        var img = new Canvas.Image; // Create a new Image
        img.src = data;

        state.canvas.width  = img.width
        state.canvas.height = img.height
        state.ctx.drawImage(img, 0, 0, img.width / 4, img.height / 4);
  });

  xhr.post(
    apiUrls.cloudVision,
    {
      json: {
        requests: [
          {
            image: {
              content: state.canvas
                .toDataURL('image/jpeg', 1)
                .replace('data:image/jpeg;base64,', '')
            },
            features: {type: 'LABEL_DETECTION', maxResults: 10}
          }
        ]
      }
    },
    (err, res, body) => {
      let labels
      if (err || !body.responses || !body.responses.length || !body.responses[0].labelAnnotations) {
        labels = []
      } else {
        labels = body.responses[0].labelAnnotations
      }
      console.log("Response Data : %o",labels)
      send('translate', labels, done)
      setTimeout(send.bind(null, 'endSnap', done), 200)
    }
  )
}
