// Insert your own API key here:
const googleKey = '@@@@@'

export const apiUrls = {
  cloudVision: 'https://deepjava.herokuapp.com/api/detect',
  translate:   'https://www.googleapis.com/language/translate/v2?key=' + googleKey
}

const queryLangs = window.location.search.slice(1)

export const langList = queryLangs
  ? queryLangs.split(',')
  : [
      'japanese',
      'french'
    ]
